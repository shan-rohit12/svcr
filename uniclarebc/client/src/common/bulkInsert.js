const { FileSystemWallet, Gateway } = require('fabric-network')
const fs = require('fs')
const path = require('path')

const ccpPath = './connection.json'
const ccpJSON = fs.readFileSync(ccpPath, 'utf8')
const ccp = JSON.parse(ccpJSON)
const connection = require('./connect.js');
var sh = require("shorthash");

let wallet_arr = [
    { "type": "X509",
  "mspId": "LordshireMSP",
  "certificate": "-----BEGIN CERTIFICATE-----\nMIICpTCCAkugAwIBAgIUbETm2JP8CCDWFI6SVz1P9oTJzF0wCgYIKoZIzj0EAwIw\nfTELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh\nbiBGcmFuY2lzY28xHjAcBgNVBAoTFUxvcmRzaGlyZS5leGFtcGxlLmNvbTEhMB8G\nA1UEAxMYY2EuTG9yZHNoaXJlLmV4YW1wbGUuY29tMB4XDTE5MTAwOTA2MDkwMFoX\nDTIwMTAwODA2MTQwMFowSDEwMA0GA1UECxMGY2xpZW50MAsGA1UECxMEb3JnMTAS\nBgNVBAsTC2RlcGFydG1lbnQxMRQwEgYDVQQDEwtBSVNIV0FSWUEgSzBZMBMGByqG\nSM49AgEGCCqGSM49AwEHA0IABPprrVkmJTgv5FZzgwhpmFw8W7eH9OcSQ64GFVTD\n7S1xkwleskSiyYtiyobRHne10KOFNbMcBnV91znBS1vV5Hujgd0wgdowDgYDVR0P\nAQH/BAQDAgeAMAwGA1UdEwEB/wQCMAAwHQYDVR0OBBYEFEgE8FTd/vHSTBcwxVBp\nnFLCqsuJMCsGA1UdIwQkMCKAIPWBoW3zJfs8Hhn9OHP1xWNkGwJFvQRLTGFwVWqZ\naUSzMG4GCCoDBAUGBwgBBGJ7ImF0dHJzIjp7ImhmLkFmZmlsaWF0aW9uIjoib3Jn\nMS5kZXBhcnRtZW50MSIsImhmLkVucm9sbG1lbnRJRCI6IkFJU0hXQVJZQSBLIiwi\naGYuVHlwZSI6ImNsaWVudCJ9fTAKBggqhkjOPQQDAgNIADBFAiEAmDu7oNUJfNgG\n9gZ4r6876Ir2b+IG5UCndYLHFKMOIdYCIBCwvj4eTKbzOLXdOxnu4G3rhX18U/Dl\nR4hKIWjSu4V0\n-----END CERTIFICATE-----\n",
  "privateKey": "-----BEGIN PRIVATE KEY-----\r\nMIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgvJXf1vb7s+kO5Cn2\r\nt1oJ+a0WZsOeAyJplPcvEin35YmhRANCAAT6a61ZJiU4L+RWc4MIaZhcPFu3h/Tn\r\nEkOuBhVUw+0tcZMJXrJEosmLYsqG0R53tdCjhTWzHAZ1fdc5wUtb1eR7\r\n-----END PRIVATE KEY-----\r\n",
  "orgName": "Lordshire",
  "userName": "AISHWARYA K",
  "contact": "9844996238",
  "address": "45/33, 4th Main,,4th Block, T.R. Nagar,Bangalore",
  "gender": "F",
  "dob": "6/29/2000",
  "fatherName": "KUMARESAN S",
  "motherName": "VIJAYALAKSHMI K",
  "photoPath": "student_photos/1001/BA1/1001_BA1_54118018609._k__93",
  "wallet_type": "Student",
  "studentId": "1D1m4M" },
{ "type": "X509",
  "mspId": "LordshireMSP",
  "certificate": "-----BEGIN CERTIFICATE-----\nMIICqjCCAlGgAwIBAgIUF+TrD/porOu5Z6Ip3QuYaE7AFXswCgYIKoZIzj0EAwIw\nfTELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh\nbiBGcmFuY2lzY28xHjAcBgNVBAoTFUxvcmRzaGlyZS5leGFtcGxlLmNvbTEhMB8G\nA1UEAxMYY2EuTG9yZHNoaXJlLmV4YW1wbGUuY29tMB4XDTE5MTAwOTA2MDkwMFoX\nDTIwMTAwODA2MTQwMFowSzEwMA0GA1UECxMGY2xpZW50MAsGA1UECxMEb3JnMTAS\nBgNVBAsTC2RlcGFydG1lbnQxMRcwFQYDVQQDEw5BTUlUSEEgTkFZQUsgTjBZMBMG\nByqGSM49AgEGCCqGSM49AwEHA0IABP0gxnj7iAdJ8Xr6xI3lEvR8SnIIh5o7OQcE\nBNY+nZ8IJCKgUOIYopnSKAQo37Aj6naQ4pVjv0WqN+WoLvGDhzOjgeAwgd0wDgYD\nVR0PAQH/BAQDAgeAMAwGA1UdEwEB/wQCMAAwHQYDVR0OBBYEFL1cpbWkQsDj5dQH\nv/4Gu6aPAW+wMCsGA1UdIwQkMCKAIPWBoW3zJfs8Hhn9OHP1xWNkGwJFvQRLTGFw\nVWqZaUSzMHEGCCoDBAUGBwgBBGV7ImF0dHJzIjp7ImhmLkFmZmlsaWF0aW9uIjoi\nb3JnMS5kZXBhcnRtZW50MSIsImhmLkVucm9sbG1lbnRJRCI6IkFNSVRIQSBOQVlB\nSyBOIiwiaGYuVHlwZSI6ImNsaWVudCJ9fTAKBggqhkjOPQQDAgNHADBEAiAs6wNM\nStx+ixJ6PD9oMbGiL4TzTOZ6/kxLSl+K/Q8kEQIgJINtKHuj+X1HN8S9C5TFxoSg\n4/Frko+cT2w1oFd1Zuw=\n-----END CERTIFICATE-----\n",
  "privateKey": "-----BEGIN PRIVATE KEY-----\r\nMIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQg7vkbLingsMZbPaqj\r\nQn3403IF+5gLfL43eMAKAX+eTeGhRANCAAT9IMZ4+4gHSfF6+sSN5RL0fEpyCIea\r\nOzkHBATWPp2fCCQioFDiGKKZ0igEKN+wI+p2kOKVY79FqjflqC7xg4cz\r\n-----END PRIVATE KEY-----\r\n",
  "orgName": "Lordshire",
  "userName": "AMITHA NAYAK N",
  "contact": "9606223392",
  "address": "NO.16/35 MOUNT JOY ROAD,HANUMANTHA NAGAR,BANGALORE",
  "gender": "F",
  "dob": "6/1/2001",
  "fatherName": "NARASIMHA NAYAK",
  "motherName": "SUDHA NAYAK",
  "photoPath": "student_photos/1001/BA1/1001_BA1_54118021301._n_43",
  "wallet_type": "Student",
  "studentId": "2kmNwi" },
{ "type": "X509",
  "mspId": "LordshireMSP",
  "certificate": "-----BEGIN CERTIFICATE-----\nMIICpzCCAk2gAwIBAgIUIev/Gk4y6rjNVKuZvdSGUIM/me4wCgYIKoZIzj0EAwIw\nfTELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh\nbiBGcmFuY2lzY28xHjAcBgNVBAoTFUxvcmRzaGlyZS5leGFtcGxlLmNvbTEhMB8G\nA1UEAxMYY2EuTG9yZHNoaXJlLmV4YW1wbGUuY29tMB4XDTE5MTAwOTA2MDkwMFoX\nDTIwMTAwODA2MTQwMFowSTEwMA0GA1UECxMGY2xpZW50MAsGA1UECxMEb3JnMTAS\nBgNVBAsTC2RlcGFydG1lbnQxMRUwEwYDVQQDEwxBTklMIEtVTUFSIFIwWTATBgcq\nhkjOPQIBBggqhkjOPQMBBwNCAATLawkfVPRxih6YuSbuxkryptCRV8De/Pbb8mJG\nwp4/sDnVrMgAYY0Zjvtc9Cst/qxXqrDMgZtqULZndkbrzsMio4HeMIHbMA4GA1Ud\nDwEB/wQEAwIHgDAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBQNia0z60PJiFV7y+W1\nGcj7qgDELzArBgNVHSMEJDAigCD1gaFt8yX7PB4Z/Thz9cVjZBsCRb0ES0xhcFVq\nmWlEszBvBggqAwQFBgcIAQRjeyJhdHRycyI6eyJoZi5BZmZpbGlhdGlvbiI6Im9y\nZzEuZGVwYXJ0bWVudDEiLCJoZi5FbnJvbGxtZW50SUQiOiJBTklMIEtVTUFSIFIi\nLCJoZi5UeXBlIjoiY2xpZW50In19MAoGCCqGSM49BAMCA0gAMEUCIQCbHGaU4io/\nsKS0kRzNsxWTozto8sqnyXn8VkwToXmTfwIgasxpc6mU87Qk49MhEXMtfuHSYsuH\nuCwKoWjUEMv14nU=\n-----END CERTIFICATE-----\n",
  "privateKey": "-----BEGIN PRIVATE KEY-----\r\nMIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgI2fCt76BQmdwj7hx\r\nl5IN/dHEzY0DVJOfK/Oo5LGBdxmhRANCAATLawkfVPRxih6YuSbuxkryptCRV8De\r\n/Pbb8mJGwp4/sDnVrMgAYY0Zjvtc9Cst/qxXqrDMgZtqULZndkbrzsMi\r\n-----END PRIVATE KEY-----\r\n",
  "orgName": "Lordshire",
  "userName": "ANIL KUMAR R",
  "contact": "7022615809",
  "address": "NO.40/1 5TH CROSS RAGAVENDRA BLOCK,SRINAGAR,BANGALORE",
  "gender": "M",
  "dob": "3/30/1995",
  "fatherName": "RAJANNA K",
  "motherName": "ROOPA C",
  "photoPath": "student_photos/1001/BA1/1001_BA1_54118016734._r__99",
  "wallet_type": "Student",
  "studentId": "2dqmX" },
{ "type": "X509",
  "mspId": "LordshireMSP",
  "certificate": "-----BEGIN CERTIFICATE-----\nMIICoDCCAkegAwIBAgIUCM2Cbu0tU4BGVF3jBBSkA10W4EowCgYIKoZIzj0EAwIw\nfTELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh\nbiBGcmFuY2lzY28xHjAcBgNVBAoTFUxvcmRzaGlyZS5leGFtcGxlLmNvbTEhMB8G\nA1UEAxMYY2EuTG9yZHNoaXJlLmV4YW1wbGUuY29tMB4XDTE5MTAwOTA2MDkwMFoX\nDTIwMTAwODA2MTQwMFowRjEwMA0GA1UECxMGY2xpZW50MAsGA1UECxMEb3JnMTAS\nBgNVBAsTC2RlcGFydG1lbnQxMRIwEAYDVQQDEwlBU0hXSU5JIE0wWTATBgcqhkjO\nPQIBBggqhkjOPQMBBwNCAAR9v/EXRjS3Y7SwagLdI7hGOg1oshiBds5FfKM3w6aC\nKYSqDxcDDLHfCho7kIGZnHHvEPBtgVz9GaYWQK0DgMDjo4HbMIHYMA4GA1UdDwEB\n/wQEAwIHgDAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBTWFLYshgrCWZ1bxKxmkgoi\nPxv+ozArBgNVHSMEJDAigCD1gaFt8yX7PB4Z/Thz9cVjZBsCRb0ES0xhcFVqmWlE\nszBsBggqAwQFBgcIAQRgeyJhdHRycyI6eyJoZi5BZmZpbGlhdGlvbiI6Im9yZzEu\nZGVwYXJ0bWVudDEiLCJoZi5FbnJvbGxtZW50SUQiOiJBU0hXSU5JIE0iLCJoZi5U\neXBlIjoiY2xpZW50In19MAoGCCqGSM49BAMCA0cAMEQCIAaMvCh8xvINPrLua80v\nz+J9ibd0hX0Kn6++phQlOF4mAiB8Tu5c6CfaSu96ICLIKIbEXCyW0ZQ9QQvKg7b9\nPHXo9w==\n-----END CERTIFICATE-----\n",
  "privateKey": "-----BEGIN PRIVATE KEY-----\r\nMIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgsAvfdiLCQywuzZo0\r\n8T9l4HVx6Z/9/HvXNIqw0JuUZ2ChRANCAAR9v/EXRjS3Y7SwagLdI7hGOg1oshiB\r\nds5FfKM3w6aCKYSqDxcDDLHfCho7kIGZnHHvEPBtgVz9GaYWQK0DgMDj\r\n-----END PRIVATE KEY-----\r\n",
  "orgName": "Lordshire",
  "userName": "ASHWINI M",
  "contact": "9611030362",
  "address": "NO.106C VEERABHADRA NAGAR,BEHIND SHARADA ASHRAMA,PESET ,BSK 3 RD STAGE ",
  "gender": "F",
  "dob": "6/15/2000",
  "fatherName": "MADESHANAYAKA",
  "motherName": "CHANDRAMMA",
  "photoPath": "student_photos/1001/BA1/1001_BA1_54118016919._m__121",
  "wallet_type": "Student",
  "studentId": "Z3JtRa" },
{ "type": "X509",
  "mspId": "LordshireMSP",
  "certificate": "-----BEGIN CERTIFICATE-----\nMIICtDCCAlugAwIBAgIUHDmZ1qMOnkCFIPXFgPAHiftaToQwCgYIKoZIzj0EAwIw\nfTELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh\nbiBGcmFuY2lzY28xHjAcBgNVBAoTFUxvcmRzaGlyZS5leGFtcGxlLmNvbTEhMB8G\nA1UEAxMYY2EuTG9yZHNoaXJlLmV4YW1wbGUuY29tMB4XDTE5MTAwOTA2MDkwMFoX\nDTIwMTAwODA2MTQwMFowUDEwMA0GA1UECxMGY2xpZW50MAsGA1UECxMEb3JnMTAS\nBgNVBAsTC2RlcGFydG1lbnQxMRwwGgYDVQQDExNESEFOVVNIIE1BTExBIE5BWUFL\nMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEwId1H3dPrUKrAD8YViryUoLl3YJZ\nEeWPRL2lzIAaMXHWLhlwsU/EbaBCo5BFvNmJ2p1ubDlbOH53uNdhSvhxKqOB5TCB\n4jAOBgNVHQ8BAf8EBAMCB4AwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUtS7F5NcZ\n64LTnB5/NKLtiCOin2wwKwYDVR0jBCQwIoAg9YGhbfMl+zweGf04c/XFY2QbAkW9\nBEtMYXBVaplpRLMwdgYIKgMEBQYHCAEEansiYXR0cnMiOnsiaGYuQWZmaWxpYXRp\nb24iOiJvcmcxLmRlcGFydG1lbnQxIiwiaGYuRW5yb2xsbWVudElEIjoiREhBTlVT\nSCBNQUxMQSBOQVlBSyIsImhmLlR5cGUiOiJjbGllbnQifX0wCgYIKoZIzj0EAwID\nRwAwRAIgMPB+pZgugqsv6LACREVk//QZYPy/VMg1emcnJhVQsgYCIH9n6Yr9BzpT\n6SzZ5EQYpzYxIEw2uy3uvOW2jD1wadsa\n-----END CERTIFICATE-----\n",
  "privateKey": "-----BEGIN PRIVATE KEY-----\r\nMIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgcOuybBI3nIg+u7qA\r\nnuieQGf6XLM2cmP0gjnpmnoVsUKhRANCAATAh3Ufd0+tQqsAPxhWKvJSguXdglkR\r\n5Y9EvaXMgBoxcdYuGXCxT8RtoEKjkEW82YnanW5sOVs4fne412FK+HEq\r\n-----END PRIVATE KEY-----\r\n",
  "orgName": "Lordshire",
  "userName": "DHANUSH MALLA NAYAK",
  "contact": "8296125292",
  "address": "NO.106C BSK 3RD STAGE ,OPP. PES COLLEGE ,SHARADHA ASHRAMA ROAD",
  "gender": "M",
  "dob": "5/31/2001",
  "fatherName": "SHIVAMALLA",
  "motherName": "KANTHAMMA",
  "photoPath": "student_photos/1001/BA1/1001_BA1_54118017797.jpg",
  "wallet_type": "Student",
  "studentId": "Z14aJ0B" },
{ "type": "X509",
  "mspId": "LordshireMSP",
  "certificate": "-----BEGIN CERTIFICATE-----\nMIICnzCCAkWgAwIBAgIUV8MgkzK0ub4o7sowSgHeTv9gHqkwCgYIKoZIzj0EAwIw\nfTELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh\nbiBGcmFuY2lzY28xHjAcBgNVBAoTFUxvcmRzaGlyZS5leGFtcGxlLmNvbTEhMB8G\nA1UEAxMYY2EuTG9yZHNoaXJlLmV4YW1wbGUuY29tMB4XDTE5MTAwOTA2MDkwMFoX\nDTIwMTAwODA2MTQwMFowRTEwMA0GA1UECxMGY2xpZW50MAsGA1UECxMEb3JnMTAS\nBgNVBAsTC2RlcGFydG1lbnQxMREwDwYDVQQDEwhESVZZQUJBSTBZMBMGByqGSM49\nAgEGCCqGSM49AwEHA0IABLOASp+GBgMTr3FdPSe0IgM7behN8jPQD0Tg+O3z4P1b\nmrcdI1xJbd9LjRA4D98pwQxpA6ceF2ha6aqXu9jLWS6jgdowgdcwDgYDVR0PAQH/\nBAQDAgeAMAwGA1UdEwEB/wQCMAAwHQYDVR0OBBYEFCS8HDdocIzjbEDi6hUU8UyG\nvnfKMCsGA1UdIwQkMCKAIPWBoW3zJfs8Hhn9OHP1xWNkGwJFvQRLTGFwVWqZaUSz\nMGsGCCoDBAUGBwgBBF97ImF0dHJzIjp7ImhmLkFmZmlsaWF0aW9uIjoib3JnMS5k\nZXBhcnRtZW50MSIsImhmLkVucm9sbG1lbnRJRCI6IkRJVllBQkFJIiwiaGYuVHlw\nZSI6ImNsaWVudCJ9fTAKBggqhkjOPQQDAgNIADBFAiEAvt8z7nkYaCbwIz54Z2kZ\nWszVqDouQx7FGEIXKfmIY9QCIFXxXrellvKqn5E+eRx/PyGG/xkljgmq2jgz/eC6\nTOpW\n-----END CERTIFICATE-----\n",
  "privateKey": "-----BEGIN PRIVATE KEY-----\r\nMIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgV86CjDh281kOVP76\r\nTd9KwtRkBzKow+ABMgYRhtbFic2hRANCAASzgEqfhgYDE69xXT0ntCIDO23oTfIz\r\n0A9E4Pjt8+D9W5q3HSNcSW3fS40QOA/fKcEMaQOnHhdoWumql7vYy1ku\r\n-----END PRIVATE KEY-----\r\n",
  "orgName": "Lordshire",
  "userName": "DIVYABAI",
  "contact": "9148918649",
  "address": "NO.2234 NAGEGOWDANAPALYA,TALGHATTPURA,BANGALORE",
  "gender": "F",
  "dob": "10/6/2000",
  "fatherName": "GOVINDANAYIKA",
  "motherName": "NIRMALABAI",
  "photoPath": "student_photos/1001/BA1/1001_BA1_54118017238.jpg",
  "wallet_type": "Student",
  "studentId": "1Aku2R" },
{ "type": "X509",
  "mspId": "LordshireMSP",
  "certificate": "-----BEGIN CERTIFICATE-----\nMIICpDCCAkugAwIBAgIUNvhXEdU938LXlrzV8kYheL/cl7QwCgYIKoZIzj0EAwIw\nfTELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh\nbiBGcmFuY2lzY28xHjAcBgNVBAoTFUxvcmRzaGlyZS5leGFtcGxlLmNvbTEhMB8G\nA1UEAxMYY2EuTG9yZHNoaXJlLmV4YW1wbGUuY29tMB4XDTE5MTAwOTA2MTAwMFoX\nDTIwMTAwODA2MTUwMFowSDEwMA0GA1UECxMGY2xpZW50MAsGA1UECxMEb3JnMTAS\nBgNVBAsTC2RlcGFydG1lbnQxMRQwEgYDVQQDEwtIQVJTSElUSEEgVjBZMBMGByqG\nSM49AgEGCCqGSM49AwEHA0IABHKBDjpcOvB42lBvJKT+T6P/PFDkqc9/gNx6b6BC\ntsZmtU89gi3Lf0pOD59qHy2TfBTuWzMMfwdW3py0a5TuEBOjgd0wgdowDgYDVR0P\nAQH/BAQDAgeAMAwGA1UdEwEB/wQCMAAwHQYDVR0OBBYEFPslv5naCkpsBLriNm8c\nJ38chCj1MCsGA1UdIwQkMCKAIPWBoW3zJfs8Hhn9OHP1xWNkGwJFvQRLTGFwVWqZ\naUSzMG4GCCoDBAUGBwgBBGJ7ImF0dHJzIjp7ImhmLkFmZmlsaWF0aW9uIjoib3Jn\nMS5kZXBhcnRtZW50MSIsImhmLkVucm9sbG1lbnRJRCI6IkhBUlNISVRIQSBWIiwi\naGYuVHlwZSI6ImNsaWVudCJ9fTAKBggqhkjOPQQDAgNHADBEAiADk4Rsmv5EKgUp\nJn8o5NLE874UQ73I3gx+DhkOEsUPXQIgBVDB28Nfcm51pGF9o26cfw71QU8qUPL2\nXCcPxUeTj/s=\n-----END CERTIFICATE-----\n",
  "privateKey": "-----BEGIN PRIVATE KEY-----\r\nMIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgWpwS9rQBnMHNwRUe\r\nekS+AQTQFx0AouE6ZFyxGVcealqhRANCAARygQ46XDrweNpQbySk/k+j/zxQ5KnP\r\nf4Dcem+gQrbGZrVPPYIty39KTg+fah8tk3wU7lszDH8HVt6ctGuU7hAT\r\n-----END PRIVATE KEY-----\r\n",
  "orgName": "Lordshire",
  "userName": "HARSHITHA V",
  "contact": "9535790569",
  "address": "NO.176 4TH  CROSS TG LAYOUT,ITTAMADU ,BANGALORE",
  "gender": "F",
  "dob": "10/17/1999",
  "fatherName": "VENKATESH T",
  "motherName": "SHARADA C",
  "photoPath": "student_photos/1001/BA1/1001_BA1_54118021230._v__13",
  "wallet_type": "Student",
  "studentId": "1ES4rT" },
{ "type": "X509",
  "mspId": "LordshireMSP",
  "certificate": "-----BEGIN CERTIFICATE-----\nMIICozCCAkmgAwIBAgIUQrZJ2DtsCwLvSLKkBsxz0uB0IGMwCgYIKoZIzj0EAwIw\nfTELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh\nbiBGcmFuY2lzY28xHjAcBgNVBAoTFUxvcmRzaGlyZS5leGFtcGxlLmNvbTEhMB8G\nA1UEAxMYY2EuTG9yZHNoaXJlLmV4YW1wbGUuY29tMB4XDTE5MTAwOTA2MTAwMFoX\nDTIwMTAwODA2MTUwMFowRzEwMA0GA1UECxMGY2xpZW50MAsGA1UECxMEb3JnMTAS\nBgNVBAsTC2RlcGFydG1lbnQxMRMwEQYDVQQDEwpKQUdBREVFU0ggMFkwEwYHKoZI\nzj0CAQYIKoZIzj0DAQcDQgAE3EnrYUW+4Yq1w0l6R6G21o5/MZLs0UREXtHNmiiS\nAJ3jhByaj4IxbPyUscnzDnt9fNn3ADTCTGbCsfrYNTrLBqOB3DCB2TAOBgNVHQ8B\nAf8EBAMCB4AwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQU70xuCI0ixIEO+GAaQ2xI\nEPSAPDkwKwYDVR0jBCQwIoAg9YGhbfMl+zweGf04c/XFY2QbAkW9BEtMYXBVaplp\nRLMwbQYIKgMEBQYHCAEEYXsiYXR0cnMiOnsiaGYuQWZmaWxpYXRpb24iOiJvcmcx\nLmRlcGFydG1lbnQxIiwiaGYuRW5yb2xsbWVudElEIjoiSkFHQURFRVNIICIsImhm\nLlR5cGUiOiJjbGllbnQifX0wCgYIKoZIzj0EAwIDSAAwRQIhAJL1R0r5OuxVH30B\nrvziFmWCHgRdpuR4G16opYY0goH3AiBP4VwHZB4RDwRRp7r85kfKOisoSxczk82r\nAZvnckbjPg==\n-----END CERTIFICATE-----\n",
  "privateKey": "-----BEGIN PRIVATE KEY-----\r\nMIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgznhNbb7nLXw5Tpuw\r\nTB6ZGHLpeqK8eVXmncnuurwdgt6hRANCAATcSethRb7hirXDSXpHobbWjn8xkuzR\r\nRERe0c2aKJIAneOEHJqPgjFs/JSxyfMOe3182fcANMJMZsKx+tg1OssG\r\n-----END PRIVATE KEY-----\r\n",
  "orgName": "Lordshire",
  "userName": "JAGADEESH ",
  "contact": "9632216851",
  "address": "# 51, 3rd Cross, ,Channasandra, Uttarahalli Main Road,Bangalore",
  "gender": "M",
  "dob": "3/19/2000",
  "fatherName": "KRISHNAPPA",
  "motherName": "GOWRAMMA",
  "photoPath": "student_photos/1001/BA1/1001_BA1_54118017594._k__27",
  "wallet_type": "Student",
  "studentId": "Z2s5WH6" },
{ "type": "X509",
  "mspId": "LordshireMSP",
  "certificate": "-----BEGIN CERTIFICATE-----\nMIICqDCCAk+gAwIBAgIUH6BV4s0CQQx5Rqz+txXmWAmH1IowCgYIKoZIzj0EAwIw\nfTELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh\nbiBGcmFuY2lzY28xHjAcBgNVBAoTFUxvcmRzaGlyZS5leGFtcGxlLmNvbTEhMB8G\nA1UEAxMYY2EuTG9yZHNoaXJlLmV4YW1wbGUuY29tMB4XDTE5MTAwOTA2MTAwMFoX\nDTIwMTAwODA2MTUwMFowSjEwMA0GA1UECxMGY2xpZW50MAsGA1UECxMEb3JnMTAS\nBgNVBAsTC2RlcGFydG1lbnQxMRYwFAYDVQQDEw1KQU5BUkRIQU4gVCBDMFkwEwYH\nKoZIzj0CAQYIKoZIzj0DAQcDQgAE7mCjI0J+pA9O1YmG7V/ajOqr4faRX2EWWuU2\n01TA1ldla9McbAjCt90u41jgeJDDJdMg0iyGzcRYbfJGTy3igqOB3zCB3DAOBgNV\nHQ8BAf8EBAMCB4AwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUEe038Lr42triLrRm\nhN+K01kms+cwKwYDVR0jBCQwIoAg9YGhbfMl+zweGf04c/XFY2QbAkW9BEtMYXBV\naplpRLMwcAYIKgMEBQYHCAEEZHsiYXR0cnMiOnsiaGYuQWZmaWxpYXRpb24iOiJv\ncmcxLmRlcGFydG1lbnQxIiwiaGYuRW5yb2xsbWVudElEIjoiSkFOQVJESEFOIFQg\nQyIsImhmLlR5cGUiOiJjbGllbnQifX0wCgYIKoZIzj0EAwIDRwAwRAIgbGzrdcxy\n1SD4DH5LA7lI7jqaIcNQ3nnLtjSy8+moBsICIDNpcoL600h6YH7/IFNcbE1+JuA1\n+HMEt7A997hiyGAK\n-----END CERTIFICATE-----\n",
  "privateKey": "-----BEGIN PRIVATE KEY-----\r\nMIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgRPgz3Z1a4Sco7HcV\r\nhMr6JU1pV51n/9pE/sWNJWsqgLWhRANCAATuYKMjQn6kD07ViYbtX9qM6qvh9pFf\r\nYRZa5TbTVMDWV2Vr0xxsCMK33S7jWOB4kMMl0yDSLIbNxFht8kZPLeKC\r\n-----END PRIVATE KEY-----\r\n",
  "orgName": "Lordshire",
  "userName": "JANARDHAN T C",
  "contact": "9606227051",
  "address": "NO.52 GOPALAPPA LAYOUT,LIBRARAY ROAD ,THALAGHATTA PURA,BANGALORE",
  "gender": "M",
  "dob": "4/24/1999",
  "fatherName": "CHANDRU TC",
  "motherName": "YASHODAMMA",
  "photoPath": "student_photos/1001/BA1/1001_BA1_54118018167. t",
  "wallet_type": "Student",
  "studentId": "Z1OOLuc" },
{ "type": "X509",
  "mspId": "LordshireMSP",
  "certificate": "-----BEGIN CERTIFICATE-----\nMIICnTCCAkOgAwIBAgIULoh6dAa70FB7ZEOBX+SwcnAtNugwCgYIKoZIzj0EAwIw\nfTELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh\nbiBGcmFuY2lzY28xHjAcBgNVBAoTFUxvcmRzaGlyZS5leGFtcGxlLmNvbTEhMB8G\nA1UEAxMYY2EuTG9yZHNoaXJlLmV4YW1wbGUuY29tMB4XDTE5MTAwOTA2MTAwMFoX\nDTIwMTAwODA2MTUwMFowRDEwMA0GA1UECxMGY2xpZW50MAsGA1UECxMEb3JnMTAS\nBgNVBAsTC2RlcGFydG1lbnQxMRAwDgYDVQQDEwdLQUxQQU5BMFkwEwYHKoZIzj0C\nAQYIKoZIzj0DAQcDQgAEC6+FIoo8Cq7f9KYyR5sFGLaDV9504OWG1H3fPqXmYPWG\nGbeKQU/VSFSQtPHueP2vUrMPL1Dilx9nGZFfUIU68aOB2TCB1jAOBgNVHQ8BAf8E\nBAMCB4AwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUDFcgFxyNtll56Deb5SIR/pDM\n5/MwKwYDVR0jBCQwIoAg9YGhbfMl+zweGf04c/XFY2QbAkW9BEtMYXBVaplpRLMw\nagYIKgMEBQYHCAEEXnsiYXR0cnMiOnsiaGYuQWZmaWxpYXRpb24iOiJvcmcxLmRl\ncGFydG1lbnQxIiwiaGYuRW5yb2xsbWVudElEIjoiS0FMUEFOQSIsImhmLlR5cGUi\nOiJjbGllbnQifX0wCgYIKoZIzj0EAwIDSAAwRQIhAIgRkWMUcUnTh3fHoxotOJSz\ny04rnMHlvFYlMnQ2xv+2AiAcDshRnIcv8VJeVcnqCI4I7WyUDeSbOSwFk9EphsAF\nww==\n-----END CERTIFICATE-----\n",
  "privateKey": "-----BEGIN PRIVATE KEY-----\r\nMIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgjf2cku0TgRka23M3\r\nCF5lSVsMRU2psQraOsw7mW4ubmKhRANCAAQLr4UiijwKrt/0pjJHmwUYtoNX3nTg\r\n5YbUfd8+peZg9YYZt4pBT9VIVJC08e54/a9Ssw8vUOKXH2cZkV9QhTrx\r\n-----END PRIVATE KEY-----\r\n",
  "orgName": "Lordshire",
  "userName": "KALPANA",
  "contact": "9980450071",
  "address": "HOGSANDHER,7TH CROSS,BEGUR MAIN ROAD,BANGALORE",
  "gender": "F",
  "dob": "4/25/2001",
  "fatherName": "NAGARAJ",
  "motherName": "SAROJBAI",
  "photoPath": "student_photos/1001/BA1/1001_BA1_54118018071.jpg",
  "wallet_type": "Student",
  "studentId": "Z2nED4U" }
];

module.exports = {
    

    async insertIntoBlockchain (req,res){
          wallet_arr.map(async item=>{
             let studentData = {};

             studentData.studentName = item.userName;
             studentData.sex = item.gender;
             studentData.age = item.dob;
             studentData.contactNumber = item.contact;
             studentData.address = item.address;
             studentData.fatherName = item.fatherName;
             studentData.motherName = item.motherName;
             studentData.photoPath = item.photoPath;
             studentData.studentId = item.studentId;
            // console.log(studentData);
            let result = await enrollStudentToBlockchain(studentData,item);
            
          });
    },

    async bulkInsertToCollege(studentData,wallet_data){
        wallet_arr.map(async item=>{
            let studentData = {};

             studentData.studentName = item.userName;
             studentData.sex = item.gender;
             studentData.age = item.dob;
             studentData.contactNumber = item.contact;
             studentData.address = item.address;
             studentData.fatherName = item.fatherName;
             studentData.motherName = item.motherName;
             studentData.photoPath = item.photoPath;
             studentData.studentId = item.studentId;

             let result = await enrollStudentToCollege(studentData,item);
             console.log(result);
        })
    }
}



async function enrollStudentToBlockchain(studentData,wallet_data) 
  {
    try {
         //console.log("wallet_data in index",wallet_data);

         
         let contract = await connection.get_contract(wallet_data);
         let {studentName,sex,age,contactNumber,address,fatherName,motherName,photoPath,studentId} = studentData;
         let result = await contract.submitTransaction(
          'enrollStudentToBlockchain',
          studentName,
          sex,
          age,
          contactNumber,
          address,
          studentId,
          fatherName,
          motherName,
          photoPath
        )
        console.log('Transaction has been submitted ' + result)
        return { status: true, data: '', msg: 'Student Added in the blockchain' }
         
    } catch (error) {
      console.log('error in user identity')
      return { status: false, data: '', msg: error }
    }
  }

  async function enrollStudentToCollege (studentData, wallet_data) {
    try {
      let { studentName, sex,  age, contactNumber,address,studentId ,fatherName,motherName,photoPath} = studentData;
      

      let contract = await connection.get_contract(wallet_data);
      
      let result = await contract.submitTransaction(
        'enrollStudentToCollege',
        studentName,
        sex,
        age,
        contactNumber,
        address,
        studentId,
        fatherName,motherName,photoPath
      )
      console.log('Transaction has been submitted ' + result)
      // await remove_wallet(wallet_data.userName);
      return { status: true, data: '', msg: 'Student Added' }
    } catch (error) {
      console.log('error in user identity')
      return { status: false, data: '', msg: error }
    }
  }