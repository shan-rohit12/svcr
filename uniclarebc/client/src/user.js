
const FabricCAServices = require('fabric-ca-client');
const fs = require('fs');
const path = require('path');

const { FileSystemWallet, Gateway, X509WalletMixin } = require('fabric-network');
const ccpPath = './connection.json';
const ccpJSON = fs.readFileSync(ccpPath, 'utf8');
const ccp = JSON.parse(ccpJSON);
var sh = require("shorthash");

const walletPath = path.join(process.cwd(), 'wallet');
const wallet = new FileSystemWallet(walletPath);



module.exports = {



    async enrollUser(req, res) {
       
        if(req.body.orgname )
        var header_data=req.body;
        if(req.query.orgname)
        var header_data = req.query;
           
        
        
        
        let orgName = header_data.orgname;
        let userName= header_data.username;
        let gender  = header_data.gender;
        let contact = header_data.contact;
        let address = header_data.address;
        let dob     = header_data.dob;
        let wallet_type = header_data.wallet_type;
        let fatherName = header_data.fatherName;
        let motherName = header_data.motherName;
        let photoPath = header_data.photoPath;
       

        
        if(orgName===undefined || userName===undefined || orgName.length==0 || userName.length==0) 
        {
            console.log("Aya");
            res.json({status:false,data:"",msg:"orgName and userName both are required!"});
            return;
        } 

        let orgMSP = ""; let ca_url = ""; let admin_identity_name =""; let dept = "";

        if (orgName.localeCompare("Lordshire") == 0) {
            orgMSP = "LordshireMSP"; ca_url = "ca1.example.com" ; admin_identity_name = "Lordshire_admin";
        
        }
        else if (orgName.localeCompare("Student") == 0) {
            orgMSP = "StudentMSP"; ca_url = "ca2.example.com";  admin_identity_name = "Student_admin";
           
        }
        else if (orgName.localeCompare("College") == 0) {
            orgMSP = "CollegeMSP"; ca_url = "ca3.example.com"; admin_identity_name = "College_admin"
        }
        else if (orgName.localeCompare("Employer") == 0) { 
            orgMSP = "EmployerMSP"; ca_url = "ca4.example.com"; admin_identity_name = "Employer_admin" 
        }
        else
        {
            console.log("Error orgname");
            res.json({stauts:false,data:"",msg:"orgName is not supplied or it is incorrect. Supported orgName are Lordshire, Student, College, Employer"});
        }

        const walletPath = path.join(process.cwd(), 'wallet');
        const wallet = new FileSystemWallet(walletPath);
         const gateway = new Gateway();
         await gateway.connect(ccp, { wallet, identity: admin_identity_name, discovery: { enabled: false } });

         const ca = gateway.getClient().getCertificateAuthority();
         const adminIdentity = gateway.getCurrentIdentity();
 
         try{
            const secret = await ca.register({ affiliation:'org1.department1', enrollmentID: userName, role: 'client' }, adminIdentity);
            const enrollment = await ca.enroll({ enrollmentID: userName, enrollmentSecret: secret });
            let userIdentity = X509WalletMixin.createIdentity(orgMSP, enrollment.certificate, enrollment.key.toBytes());
            // console.log(userIdentity);
            // console.log(typeof userIdentity);
           // console.log("certificate->",userIdentity.certificate);
            wallet.import(userName, userIdentity);
            // add orgname and userName
            userIdentity.orgName = orgName;
            userIdentity.userName= userName;
            
            userIdentity.contact =contact  ;         
            userIdentity.address = address;
            
            if(wallet_type==="Student")
            {
            userIdentity.gender = gender;
            userIdentity.dob = dob;
            userIdentity.fatherName = fatherName;
            userIdentity.motherName = motherName;
            userIdentity.photoPath = photoPath;
            }
            userIdentity.wallet_type = wallet_type;
            //userIdentity = JSON.stringify(userIdentity);
            userIdentity.studentId = sh.unique(userIdentity.certificate);
            res.json({status:true,data:userIdentity});
         }
         catch(error){
             console.log(error);
             res.json({status:false,data:"",msg:error});
         }
         
    },

    async userInsert()
    {
       
        let arr = [
            {
              "fdegree": "BA1",
              "fdescpn": "Bachelor of Arts",
              "fcollcode": 1001,
              "fcollname": "Acharya Patashala College of Arts and Science , N. R. Colony",
              "fregno": "A1810001",
              "username": "AISHWARYA K",
              "fatherName": "KUMARESAN S",
              "motherName": "VIJAYALAKSHMI K",
              "gender": "F",
              "dob": "6/29/2000",
              "fcaste": "IIA",
              "femail": "984499acchu@gmail.com",
              "contact": 9844996238,
              "address": "45/33, 4th Main,,4th Block, T.R. Nagar,Bangalore",
              "photoPath": "student_photos/1001/BA1/1001_BA1_54118018609._k__93",
              "mcfilename": "MC_041_A1810001_A_2018_2.pdf"
            },
            {
              "fdegree": "BA1",
              "fdescpn": "Bachelor of Arts",
              "fcollcode": 1001,
              "fcollname": "Acharya Patashala College of Arts and Science , N. R. Colony",
              "fregno": "A1810002",
              "username": "AMITHA NAYAK N",
              "fatherName": "NARASIMHA NAYAK",
              "motherName": "SUDHA NAYAK",
              "gender": "F",
              "dob": "6/1/2001",
              "fcaste": "GM",
              "femail": "amitha012001@gmail.com",
              "contact": 9606223392,
              "address": "NO.16/35 MOUNT JOY ROAD,HANUMANTHA NAGAR,BANGALORE",
              "photoPath": "student_photos/1001/BA1/1001_BA1_54118021301._n_43",
              "mcfilename": "MC_041_A1810002_A_2018_2.pdf"
            },
            {
              "fdegree": "BA1",
              "fdescpn": "Bachelor of Arts",
              "fcollcode": 1001,
              "fcollname": "Acharya Patashala College of Arts and Science , N. R. Colony",
              "fregno": "A1810003",
              "username": "ANIL KUMAR R",
              "fatherName": "RAJANNA K",
              "motherName": "ROOPA C",
              "gender": "M",
              "dob": "3/30/1995",
              "fcaste": "IIA",
              "femail": "13ncjc30@gmail.com",
              "contact": 7022615809,
              "address": "NO.40/1 5TH CROSS RAGAVENDRA BLOCK,SRINAGAR,BANGALORE",
              "photoPath": "student_photos/1001/BA1/1001_BA1_54118016734._r__99",
              "mcfilename": "MC_041_A1810003_A_2018_2.pdf"
            },
            {
              "fdegree": "BA1",
              "fdescpn": "Bachelor of Arts",
              "fcollcode": 1001,
              "fcollname": "Acharya Patashala College of Arts and Science , N. R. Colony",
              "fregno": "A1810004",
              "username": "ASHWINI M",
              "fatherName": "MADESHANAYAKA",
              "motherName": "CHANDRAMMA",
              "gender": "F",
              "dob": "6/15/2000",
              "fcaste": "ST",
              "femail": "ashwinianu76@gmail.com",
              "contact": 9611030362,
              "address": "NO.106C VEERABHADRA NAGAR,BEHIND SHARADA ASHRAMA,PESET ,BSK 3 RD STAGE ",
              "photoPath": "student_photos/1001/BA1/1001_BA1_54118016919._m__121",
              "mcfilename": "MC_041_A1810004_A_2018_2.pdf"
            },
            {
              "fdegree": "BA1",
              "fdescpn": "Bachelor of Arts",
              "fcollcode": 1001,
              "fcollname": "Acharya Patashala College of Arts and Science , N. R. Colony",
              "fregno": "A1810005",
              "username": "DHANUSH MALLA NAYAK",
              "fatherName": "SHIVAMALLA",
              "motherName": "KANTHAMMA",
              "gender": "M",
              "dob": "5/31/2001",
              "fcaste": "ST",
              "femail": "dhanushdhanu1398@gmail.com",
              "contact": 8296125292,
              "address": "NO.106C BSK 3RD STAGE ,OPP. PES COLLEGE ,SHARADHA ASHRAMA ROAD",
              "photoPath": "student_photos/1001/BA1/1001_BA1_54118017797.jpg",
              "mcfilename": "MC_041_A1810005_A_2018_2.pdf"
            },
            {
              "fdegree": "BA1",
              "fdescpn": "Bachelor of Arts",
              "fcollcode": 1001,
              "fcollname": "Acharya Patashala College of Arts and Science , N. R. Colony",
              "fregno": "A1810006",
              "username": "DIVYABAI",
              "fatherName": "GOVINDANAYIKA",
              "motherName": "NIRMALABAI",
              "gender": "F",
              "dob": "10/6/2000",
              "fcaste": "SC",
              "femail": "baidivya610@gmail.com",
              "contact": 9148918649,
              "address": "NO.2234 NAGEGOWDANAPALYA,TALGHATTPURA,BANGALORE",
              "photoPath": "student_photos/1001/BA1/1001_BA1_54118017238.jpg",
              "mcfilename": "MC_041_A1810006_A_2018_2.pdf"
            },
            {
              "fdegree": "BA1",
              "fdescpn": "Bachelor of Arts",
              "fcollcode": 1001,
              "fcollname": "Acharya Patashala College of Arts and Science , N. R. Colony",
              "fregno": "A1810007",
              "username": "HARSHITHA V",
              "fatherName": "VENKATESH T",
              "motherName": "SHARADA C",
              "gender": "F",
              "dob": "10/17/1999",
              "fcaste": "IIIA",
              "femail": "harshithagombe9535@gmail.com",
              "contact": 9535790569,
              "address": "NO.176 4TH  CROSS TG LAYOUT,ITTAMADU ,BANGALORE",
              "photoPath": "student_photos/1001/BA1/1001_BA1_54118021230._v__13",
              "mcfilename": "MC_041_A1810007_A_2018_2.pdf"
            },
            {
              "fdegree": "BA1",
              "fdescpn": "Bachelor of Arts",
              "fcollcode": 1001,
              "fcollname": "Acharya Patashala College of Arts and Science , N. R. Colony",
              "fregno": "A1810008",
              "username": "JAGADEESH ",
              "fatherName": "KRISHNAPPA",
              "motherName": "GOWRAMMA",
              "gender": "M",
              "dob": "3/19/2000",
              "fcaste": "SC",
              "femail": "jagij0333@gmail.com",
              "contact": 9632216851,
              "address": "# 51, 3rd Cross, ,Channasandra, Uttarahalli Main Road,Bangalore",
              "photoPath": "student_photos/1001/BA1/1001_BA1_54118017594._k__27",
              "mcfilename": "MC_041_A1810008_A_2018_2.pdf"
            },
            {
              "fdegree": "BA1",
              "fdescpn": "Bachelor of Arts",
              "fcollcode": 1001,
              "fcollname": "Acharya Patashala College of Arts and Science , N. R. Colony",
              "fregno": "A1810009",
              "username": "JANARDHAN T C",
              "fatherName": "CHANDRU TC",
              "motherName": "YASHODAMMA",
              "gender": "M",
              "dob": "4/24/1999",
              "fcaste": "SC",
              "femail": "janardhan74013@gmail.com",
              "contact": 9606227051,
              "address": "NO.52 GOPALAPPA LAYOUT,LIBRARAY ROAD ,THALAGHATTA PURA,BANGALORE",
              "photoPath": "student_photos/1001/BA1/1001_BA1_54118018167. t",
              "mcfilename": "MC_041_A1810009_A_2018_2.pdf"
            },
            {
              "fdegree": "BA1",
              "fdescpn": "Bachelor of Arts",
              "fcollcode": 1001,
              "fcollname": "Acharya Patashala College of Arts and Science , N. R. Colony",
              "fregno": "A1810010",
              "username": "KALPANA",
              "fatherName": "NAGARAJ",
              "motherName": "SAROJBAI",
              "gender": "F",
              "dob": "4/25/2001",
              "fcaste": "SC",
              "femail": "kirankumarkiran66906@gmail.com",
              "contact": 9980450071,
              "address": "HOGSANDHER,7TH CROSS,BEGUR MAIN ROAD,BANGALORE",
              "photoPath": "student_photos/1001/BA1/1001_BA1_54118018071.jpg",
              "mcfilename": "MC_041_A1810010_A_2018_2.pdf"
            }
           ];
        
        

       
       
         for(var i=0;i<arr.length;i++)
         {

            let orgName = "Lordshire";
            let userName= arr[i].username;
            let gender  = arr[i].gender;
            let contact = arr[i].contact;
            let address = arr[i].address;
            let dob     = arr[i].dob;
            let wallet_type = "Student";
            let fatherName = arr[i].fatherName;
            let motherName = arr[i].motherName;
            let photoPath = arr[i].photoPath;
        
        if(orgName===undefined || userName===undefined || orgName.length==0 || userName.length==0) 
        {
            console.log("Aya");
            res.json({status:false,data:"",msg:"orgName and userName both are required!"});
            return;
        } 

        let orgMSP = ""; let ca_url = ""; let admin_identity_name =""; let dept = "";

        if (orgName.localeCompare("Lordshire") == 0) {
            orgMSP = "LordshireMSP"; ca_url = "ca1.example.com" ; admin_identity_name = "Lordshire_admin";
        
        }
        else if (orgName.localeCompare("Student") == 0) {
            orgMSP = "StudentMSP"; ca_url = "ca2.example.com";  admin_identity_name = "Student_admin";
           
        }
        else if (orgName.localeCompare("College") == 0) {
            orgMSP = "CollegeMSP"; ca_url = "ca3.example.com"; admin_identity_name = "College_admin"
        }
        else if (orgName.localeCompare("Employer") == 0) { 
            orgMSP = "EmployerMSP"; ca_url = "ca4.example.com"; admin_identity_name = "Employer_admin" 
        }
        else
        {
            console.log("Error orgname");
            res.json({stauts:false,data:"",msg:"orgName is not supplied or it is incorrect. Supported orgName are Lordshire, Student, College, Employer"});
        }

        const walletPath = path.join(process.cwd(), 'wallet');
        const wallet = new FileSystemWallet(walletPath);
         const gateway = new Gateway();
         await gateway.connect(ccp, { wallet, identity: admin_identity_name, discovery: { enabled: false } });

         const ca = gateway.getClient().getCertificateAuthority();
         const adminIdentity = gateway.getCurrentIdentity();
 
         try{
            const secret = await ca.register({ affiliation:'org1.department1', enrollmentID: userName, role: 'client' }, adminIdentity);
            const enrollment = await ca.enroll({ enrollmentID: userName, enrollmentSecret: secret });
            let userIdentity = X509WalletMixin.createIdentity(orgMSP, enrollment.certificate, enrollment.key.toBytes());
            // console.log(userIdentity);
            // console.log(typeof userIdentity);
           // console.log("certificate->",userIdentity.certificate);
           userIdentity.orgName = orgName;
            userIdentity.userName= userName;
            
            userIdentity.contact =contact  ;         
            userIdentity.address = address;
            
            if(wallet_type==="Student")
            {
            userIdentity.gender = gender;
            userIdentity.dob = dob;
            userIdentity.fatherName = fatherName;
            userIdentity.motherName = motherName;
            userIdentity.photoPath = photoPath;
            }
            userIdentity.wallet_type = wallet_type;
            userIdentity.studentId = sh.unique(userIdentity.certificate);
            await wallet.import(userName, userIdentity);
            console.log(userIdentity);
            // add orgname and userName
            
            //userIdentity = JSON.stringify(userIdentity);
            
            //res.json({status:true,data:userIdentity});
             
         }
         catch(error){
             console.log(error);
             res.json({status:false,data:"",msg:error});
         }

        }
         
    }

};